from flask_testing import TestCase
from app import app, db, insert_random_user, delete_user_over_minute_old
from models import Contact, Email
import json
import datetime


class BaseTestCase(TestCase):

    def create_app(self):
        app.config['TESTING'] = True
        return app

    def setUp(self):
        db.drop_all()
        db.create_all()
        # create contact over 2 mins old to test the delete_user_over_minute_old func.
        two_mins_ago = datetime.datetime.utcnow() - datetime.timedelta(minutes=2)
        user = Contact(username='mackers', surname='McVey', first_name='James', created_date=two_mins_ago)
        for email in ['test1@example.com', 'test2@example.com']:
            email_for_user = Email(email=email)
            user.contacts.append(email_for_user)
        db.session.add(user)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        db.create_all()


class TestAPI(BaseTestCase):
    def test_get_data_for_initial_contact(self):
        response = self.client.get('/contact')
        self.assert200(response)
        self.assertEqual(1, json.loads(response.data).__len__())
        for data in json.loads(response.data):
            self.assertEqual(2, data.get('email').__len__())

    def test_get_data_for_two_contacts(self):
        user = Contact(username='mackers2', surname='McVey', first_name='James')
        for email in ['test1@example.com', 'test2@example.com']:
            email_for_user = Email(email=email)
            user.contacts.append(email_for_user)
        db.session.add(user)
        db.session.commit()

        response = self.client.get('/contact')
        self.assert200(response)
        self.assertEqual(Contact.query.all().__len__(), json.loads(response.data).__len__())
        for data in json.loads(response.data):
            self.assertEqual(2, data.get('email').__len__())

    def test_get_data_for_contact(self):
        response = self.client.get('/contact/mackers')
        self.assert200(response)
        self.assertEqual('James', json.loads(response.data).get('first_name'))
        self.assertEqual('McVey', json.loads(response.data).get('surname'))
        self.assertEqual('mackers', json.loads(response.data).get('username'))
        self.assertEqual(1, json.loads(response.data).get('contact_id'))
        self.assertIn('test1@example.com', json.loads(response.data).get('email'))
        self.assertIn('test2@example.com', json.loads(response.data).get('email'))
        self.assertEqual(2, json.loads(response.data).get('email').__len__())

    def test_do_not_get_data_for_non_existant_contact(self):
        response = self.client.get('/contact/i_dont_exist')
        self.assertEqual(response.status_code, 204)

    def test_post_new_contact_with_missing_info(self):
        response = self.client.post('/contact', headers={'Content-Type': 'application/json',
                                                        'email': 'frank1@example.com,frank2@example.com,frank3@example.com'
                                                        })
        self.assert400(response)

    def test_post_new_contact(self):
        response = self.client.post('/contact', headers={'Content-Type': 'application/json',
                                                        'username': 'ratpack',
                                                        'first_name': 'Frank',
                                                        'surname': 'Sinatra',
                                                        'email': 'frank1@example.com,frank2@example.com,frank3@example.com'
                                                        })
        self.assert200(response)
        response = self.client.get('/contact/ratpack')
        self.assert200(response)
        self.assertEqual('Frank', json.loads(response.data).get('first_name'))
        self.assertEqual('Sinatra', json.loads(response.data).get('surname'))
        self.assertEqual('ratpack', json.loads(response.data).get('username'))
        self.assertEqual(2, json.loads(response.data).get('contact_id'))
        self.assertIn('frank1@example.com', json.loads(response.data).get('email'))
        self.assertIn('frank2@example.com', json.loads(response.data).get('email'))
        self.assertIn('frank3@example.com', json.loads(response.data).get('email'))
        self.assertEqual(3, json.loads(response.data).get('email').__len__())

    def test_post_existing_contact(self):
        response = self.client.post('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'mackers',
                                                         'first_name': 'James',
                                                         'surname': 'McVey',
                                                         'email': 'mac@example.com'
                                                         })
        self.assert400(response)
        self.assertEqual(response.data, b'User mackers already exists')

    def test_post_new_contact_no_email(self):
        response = self.client.post('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'ratpack',
                                                         'first_name': 'Frank',
                                                         'surname': 'Sinatra',
                                                         })
        self.assert400(response)
        self.assertEqual(response.data, b'No email provided - no contact has been created or updated')

    def test_post_new_contact_dodgy_email(self):
        response = self.client.post('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'ratpack',
                                                         'first_name': 'Frank',
                                                         'surname': 'Sinatra',
                                                         'email': 'frank1example.com'
                                                         })
        self.assert400(response)
        self.assertEqual(response.data, b'Email frank1example.com is invalid - no contact has been created or updated')

    def test_delete_contact(self):
        response = self.client.delete('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'mackers'
                                                         })
        self.assert200(response)
        response = self.client.get('/contact/mackers')
        self.assertEqual(response.status_code, 204)

    def test_delete_non_existant_contact(self):
        response = self.client.delete('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'i_dont_exist'
                                                         })
        self.assert400(response)
        self.assertEqual(b'User i_dont_exist not found', response.data)

    def test_put_existing_contact(self):
        response = self.client.put('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'mackers',
                                                         'first_name': 'Jim',
                                                         'surname': 'McVey',
                                                         'email': 'mac@example.com'
                                                         })
        self.assert200(response)
        response = self.client.get('/contact/mackers')
        self.assert200(response)
        self.assertEqual('Jim', json.loads(response.data).get('first_name'))
        self.assertEqual('McVey', json.loads(response.data).get('surname'))
        self.assertEqual('mackers', json.loads(response.data).get('username'))
        self.assertEqual(1, json.loads(response.data).get('contact_id'))
        self.assertIn('mac@example.com', json.loads(response.data).get('email'))
        self.assertEqual(1, json.loads(response.data).get('email').__len__())

    def test_put_existing_contact_no_email(self):
        response = self.client.put('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'mackers',
                                                         'first_name': 'Jim',
                                                         'surname': 'McVey'
                                                         })
        self.assert400(response)

    def test_put_existing_contact_dodgy_email(self):
        response = self.client.put('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'mackers',
                                                         'first_name': 'Jim',
                                                         'surname': 'McVey',
                                                         'email': 'dodgy_email'
                                                         })
        self.assert400(response)
        self.assertEqual(response.data, b'Email dodgy_email is invalid - no contact has been created or updated')

    def test_random_user_added(self):
        insert_random_user()
        insert_random_user()
        # Should now have 3 contacts
        contacts = Contact.query.all()
        self.assertEqual(3, contacts.__len__())

    def test_user_over_min_old_deleted(self):
        delete_user_over_minute_old()
        # Should now have 3 contacts
        contacts = Contact.query.all()
        self.assertEqual(0, contacts.__len__())
