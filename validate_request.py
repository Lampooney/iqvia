from email.utils import parseaddr


def validate_email(email):
    """

    :param email: Email to be checked
    :return: If the email is a valid email address - this is a basic check for the purposes of this exercise
    """
    return '@' in parseaddr(email)[1]


def validate_request(request):
    try:
        # Get the request header info
        username = request.headers.get('username')
        surname = request.headers.get('surname')
        first_name = request.headers.get('first_name')
        emails = request.headers.get('email', '').split(',')
        # If there is no username, surname, first_name or email then treat as 400
        if username is None:
            return 'No username provided - no contact has been created or updated', 400, None
        if surname is None:
            return 'No surname provided - no contact has been created or updated', 400, None
        if first_name is None:
            return 'No surname provided - no contact has been created or updated', 400, None
        if emails == ['']:
            return 'No email provided - no contact has been created or updated', 400, None
        # Also basic server side validation of the email
        for email in emails:
            if not validate_email(email):
                return f'Email {email} is invalid - no contact has been created or updated', 400, None

        # If all passes then return a 200 & the data as a dictionary
        return 'All okay', 200, {'username': username.lower()
                                , 'surname': surname
                                , 'first_name': first_name
                                , 'emails': emails
                                }
    except Exception as e:
        return str(e), 400, {}
