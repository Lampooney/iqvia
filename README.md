# Installation

Install Python virtual environment, create an environment called venv, initiate it and install the projects requirements

    git clone https://Lampooney@bitbucket.org/Lampooney/iqvia.git 
    cd IQVIA/iqvia/
    sudo pip3 install virtualenv
    virtualenv venv
    source ./venv/bin/activate
    pip install -r requirements.txt 
    
# Q1 & Q2

Run initial_db_create.py to initiate the SQLite DB

Run app.py to get the app running in dev mode

### GET - ALL CONTACTS

Go to http://127.0.0.1:5000/contact to get all the contacts

### GET - A CONTACT

Go to http://127.0.0.1:5000/contact/***username*** to get all the contacts

### POST

To add a user post to http://127.0.0.1:5000/contact  with the headers username, first_name, surname & email

eg 
     response = self.client.post('/contact', headers={'Content-Type': 'application/json',
                                            'username': 'ratpack',
                                            'first_name': 'Frank',
                                            'surname': 'Sinatra',
                                            'email': 'frank1@example.com,frank2@example.com,frank3@example.com'
                                            })

### PUT
To update a user put to http://127.0.0.1:5000/contact  with the headers username, first_name, surname & email

eg 
     response = self.client.put('/contact', headers={'Content-Type': 'application/json',
                                                     'username': 'mackers',
                                                     'first_name': 'Jim',
                                                     'surname': 'McVey',
                                                     'email': 'mac@example.com'
                                                     })

### DELETE
To delete a user delete to http://127.0.0.1:5000/contact  with the headers username
eg
     response = self.client.delete('/contact', headers={'Content-Type': 'application/json',
                                                         'username': 'mackers'
                                                      })

### UNIT TESTS
Tests are found in unit_tests/tests.py
    
# Running Q3

Run you redis session - the config for it is in app.py

Run the Celery worker in a terminal session

     celery worker -A app.celery --loglevel=info  
     
In python run the following commands 

     from app import insert_new_user_every_15_seconds
     task = insert_new_user_every_15_seconds.delay()
     
The results can be seen changing via [this link](http://127.0.0.1:5000/contact)

     