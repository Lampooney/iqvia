import datetime
from app import db


class Email(db.Model):
    email_id = db.Column(db.Integer, primary_key=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.contact_id'), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    contact = db.relationship('Contact', backref=db.backref('contacts', lazy=True))

    def __repr__(self):
        return '<Email %r>' % self.email


class Contact(db.Model):
    contact_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Contact %r>' % self.contact_id



