from app import db
from models import Contact, Email
db.drop_all()
db.create_all()
user = Contact(username='mackers', surname='McVey', first_name='James')
email1 = Email(email='me@me.com')
email2 = Email(email='me2@me.com')
user.contacts.append(email1)
user.contacts.append(email2)
db.session.add(user)
db.session.commit()