# coding: utf8

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from validate_request import validate_request
from celery import Celery
import uuid
import time
import datetime


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db?check_same_thread=False'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)
db = SQLAlchemy(app)

from models import Contact, Email


@app.route('/contact')
def get_contacts():
    try:
        # Get all the contacts
        users = Contact.query.all()
        user_data = []
        # for each contact get the email address(es)
        for user in users:
            # Get emails for the user
            emails = Email.query.filter_by(contact_id=user.contact_id).all()
            email_list = []
            # Add the address to a list
            for email in emails:
                email_list.append(email.email)
            # Add the data to the main list
            user_data.append(
                    {'contact_id': user.contact_id,
                     'username': user.username,
                     'first_name': user.first_name,
                     'surname': user.surname,
                     'email': email_list,
                     'created_date': user.created_date,
                     }
            )
        return jsonify(user_data), 200
    except Exception as e:
        return str(e), 400


@app.route('/contact/<username>')
def get_contact(username):
    try:
        # Get the user by the username
        user = Contact.query.filter_by(username=username).first()
        # If the username exists, get the email addresses
        if user is not None:
            # Get emails for the user
            emails = Email.query.filter_by(contact_id=user.contact_id).all()
            email_list = []
            for email in emails:
                email_list.append(email.email)
            return jsonify(
                    {'contact_id': user.contact_id,
                     'username': user.username,
                     'first_name': user.first_name,
                     'surname': user.surname,
                     'email': email_list,
                     'created_date': user.created_date,
                     }
            ), 200
        else:
            # Else return a 400
            return f'User {username} not found', 204
    except Exception as e:
        return str(e), 400


@app.route('/contact', methods=['POST'])
def new_contact():
    """
    Add a new user into the system
    :return:
    """
    try:
        # Validate the request being sent
        resp_message, resp_code, contact_info = validate_request(request)
        # If the data is okay (resp_code == 200) then proceed
        if resp_code == 200:
            username = contact_info.get('username')
            first_name = contact_info.get('first_name')
            surname = contact_info.get('surname')
            emails = contact_info.get('emails')
            user = Contact.query.filter_by(username=username).first()
            # If user doesn't exist then create
            if user is None:
                contact = Contact(username=username, surname=surname, first_name=first_name)
                for email in emails:
                    contact_email = Email(email=email)
                    contact.contacts.append(contact_email)
                db.session.add(contact)
                db.session.commit()
                return f'User {username} created', 200
            else:
                # If user already exists then return 400
                return f'User {username} already exists', 400
        else:
            return resp_message, resp_code
    except sqlalchemy.exc.IntegrityError as e:
        return str(e), 400
    except sqlalchemy.exc.InvalidRequestError as e:
        return str(e), 400
    except Exception as e:
        return str(e), 400


@app.route('/contact', methods=['PUT'])
def update_contact():
    """
    Add a new user into the system
    :return:
    """
    try:
        # Validate the request being sent
        resp_message, resp_code, contact_info = validate_request(request)
        # If the data is okay (resp_code == 200) then proceed
        if resp_code == 200:
            username = contact_info.get('username')
            first_name = contact_info.get('first_name')
            surname = contact_info.get('surname')
            emails = contact_info.get('emails')
            user = Contact.query.filter_by(username=username).first()
            # If user exists then update
            if user is not None:
                user.surname = surname
                user.first_name = first_name
                Email.query.filter_by(contact_id=user.contact_id).delete()
                for email in emails:
                    contact_email = Email(email=email, contact_id=user.contact_id)
                    db.session.add(contact_email)
                db.session.commit()
                return f'User {username} updated', 200
            # else return a 400
            else:
                return f'User {username} does not exist', 200
        else:
            return resp_message, resp_code
    except sqlalchemy.exc.IntegrityError as e:
        return str(e), 400
    except sqlalchemy.exc.InvalidRequestError as e:
        return str(e), 400
    except Exception as e:
        return str(e), 400


@app.route('/contact', methods=['DELETE'])
def delete_contact():
    try:
        # Get the user
        username = request.headers.get('username')
        user = Contact.query.filter_by(username=username).first()
        # If the user exists then delete
        if user is not None:
            Email.query.filter_by(contact_id=user.contact_id).delete()
            Contact.query.filter_by(contact_id=user.contact_id).delete()
            db.session.commit()
            return f'User {username} deleted', 200
        # Else return 400
        else:
            return f'User {username} not found', 400
    except Exception as e:
        return str(e), 400


@celery.task
def insert_new_user_every_15_seconds():
    while True:
        # Insert a random user
        insert_random_user()
        # Delete users over a minute old
        delete_user_over_minute_old()
        time.sleep(15)


def insert_random_user():
    username = str(uuid.uuid4()).replace('-', '')
    first_name = 'Jimmy'
    surname = 'Mac'
    email1 = Email(email=f'{username}_1.example.com')
    email2 = Email(email=f'{username}_2.example.com')
    contact = Contact(username=username, surname=surname, first_name=first_name)
    contact.contacts.append(email1)
    contact.contacts.append(email2)
    db.session.add(contact)
    db.session.commit()


def delete_user_over_minute_old():
    min_ago = datetime.datetime.utcnow() - datetime.timedelta(minutes=1)
    contacts = Contact.query.filter(Contact.created_date < min_ago).all()

    for contact in contacts:
        Email.query.filter_by(contact_id=contact.contact_id).delete()
        Contact.query.filter_by(contact_id=contact.contact_id).delete()
        db.session.commit()


if __name__ == '__main__':
    app.run()
